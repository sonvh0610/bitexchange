import md5 from 'md5';
import { errorHandler } from './errorHandler';

const urlEndpoint = 'https://secure.bitexchangecoin.com/modules/api.php?do='

export class ApiInterface {
	static login = (username, passwordHashedToMd5) => {
		var formData = new FormData();
		formData.append('username', username);
		formData.append('password', md5(passwordHashedToMd5));
		return fetch(`${urlEndpoint}loginApi`, {
			method: 'POST',
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'multipart/form-data'
			},
			body: formData
		})
		.then(errorHandler.checkStatusAndParseJson)
		.then(errorHandler.validAccount);
	}

	static logout = (accessToken) => {
		var formData = new FormData();
		formData.append('token', accessToken);

		return fetch(`${urlEndpoint}logoutApi`, {
			method: 'POST',
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'multipart/form-data'
			},
			body: formData
		})
		.then(errorHandler.checkStatusAndParseJson)
		.then(errorHandler.logout);
	}

	static getUserInfo = (token) => {
		var formData = new FormData();
		formData.append('token', token);

		return fetch(`${urlEndpoint}getUserInfo`, {
			method: 'POST',
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'multipart/form-data'
			},
			body: formData
		})
		.then(errorHandler.checkStatusAndParseJson)
		.then(errorHandler.getUserInfo);
	}

	static sendBecCoin(accessToken, userPassword, transactionPassword, receiver, amount) {
		var formData = new FormData();
		formData.append('token', accessToken);
		formData.append('password', userPassword);
		formData.append('transactionPassword', md5(transactionPassword));
		formData.append('sendToUser', receiver);
		formData.append('bec', amount);

		return fetch(`${urlEndpoint}sendBecApi`, {
			method: 'POST',
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'multipart/form-data'
			},
			body: formData
		})
		.then(errorHandler.checkStatusAndParseJson)
		.then(errorHandler.sendBec);
	}

	static viewHistorySent(accessToken, startDate, endDate) {
		var formData = new FormData();
		let startDateUnix = parseInt((startDate.getTime() / 1000).toFixed(0));
		let endDateUnix = parseInt((endDate.getTime() / 1000).toFixed(0));

		formData.append('token', accessToken);
		formData.append('startDate', startDateUnix);
		formData.append('endDate', endDateUnix);

		return fetch(`${urlEndpoint}historySent`, {
			method: 'POST',
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'multipart/form-data'
			},
			body: formData
		})
		.then(errorHandler.checkStatusAndParseJson)
		.then(errorHandler.viewHistory);
	}

	static viewHistoryReceived(accessToken, startDate, endDate) {
		var formData = new FormData();
		let startDateUnix = parseInt((startDate.getTime() / 1000).toFixed(0));
		let endDateUnix = parseInt((endDate.getTime() / 1000).toFixed(0));

		formData.append('token', accessToken);
		formData.append('startDate', startDateUnix);
		formData.append('endDate', endDateUnix);

		return fetch(`${urlEndpoint}historyReceived`, {
			method: 'POST',
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'multipart/form-data'
			},
			body: formData
		})
		.then(errorHandler.checkStatusAndParseJson)
		.then(errorHandler.viewHistory);
	}
}