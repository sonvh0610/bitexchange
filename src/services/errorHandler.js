import { RESPONSE, ERROR_MESSAGE } from '../constants';

export const errorHandler = {
	checkStatusAndParseJson: function(response) {
		if (response.status >= 200 && response.status < 300) {
			return response.json();
		}
		else {
			throw new Error(ERROR_MESSAGE.SYSTEM_CRASH);
		}
	},
	validAccount: function(response) {
		if (response.error !== RESPONSE.ERROR) {
			return response.token;
		}
		else {
			throw new Error(ERROR_MESSAGE.USERNAME_PASSWORD_INVALID);
		}
	},
	logout: function(response) {
		if (response.error != RESPONSE.ERROR) {
			return response.logout;
		}
		else {
			throw new Error(ERROR_MESSAGE.LOGOUT_FAILURE);
		}
	},
	getUserInfo: function(response) {
		if (response.error !== RESPONSE.ERROR) {
			return response.data;
		}
		else {
			throw new Error(ERROR_MESSAGE.FAILURE_WHEN_GET_USER);
		}
	},
	sendBec: function(response) {
		if (response.error !== RESPONSE.ERROR) {
			return response.sendBecApi;
		}
		else {
			throw new Error(response.message);
		}
	},
	viewHistory: function(response) {
		if (response.error !== RESPONSE.ERROR) {
			return response.data;
		}
		else {
			throw new Error(response.message);
		}
	}
};