import { DASHBOARD } from '../constants';

export default (state = {}, { type, payload }) => {
	switch (type) {
		case DASHBOARD.GET_USER_PROCESS: {
			return {
				activeLoadingIndicator: true
			}
		}
		case DASHBOARD.GET_USER_SUCCESS: {
			return {
				activeLoadingIndicator: false,
				fetchUser: true,
				userInfo: payload.userInfo
			}
		}
		case DASHBOARD.GET_USER_FAILED: {
			return {
				activeLoadingIndicator: false,
				fetchUser: false,
				errorMessage: payload.errorMessage
			}
		}
		case DASHBOARD.LOGOUT_PROCESS: {
			return {
				activeLoadingIndicator: true
			}
		}
		case DASHBOARD.LOGOUT_SUCCESS: {
			return {
				activeLoadingIndicator: false
			}
		}
		case DASHBOARD.LOGOUT_FAILURE: {
			return {
				activeLoadingIndicator: false,
				errorMessage: payload.errorMessage
			}
		}
		default:
			return state;
	}	
};