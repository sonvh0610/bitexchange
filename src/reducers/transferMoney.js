import { TRANSER_MONEY } from '../constants';

const DEFAULT_STATE = {
    activeLoadingIndicator: false
}

export default (state = DEFAULT_STATE, { type, payload }) => {
    switch (type) {
        case TRANSER_MONEY.PROGRESS: {
			return {
				activeLoadingIndicator: true
			};
		}
		case TRANSER_MONEY.SUCCESS: {
			return {
                activeLoadingIndicator: false,
                transferMoneyStatus: true
			};
		}
		case TRANSER_MONEY.FAILURE: {
			return {
                activeLoadingIndicator: false,
                transferMoneyStatus: false,
                errorMessage: payload.errorMessage
			};
		}
		default:
			return state;
    }
};