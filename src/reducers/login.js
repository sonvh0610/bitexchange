import { LOGIN } from '../constants';

const DEFAULT_STATE = {
	activeLoadingIndicator: false
};

export default (state = DEFAULT_STATE, { type, payload }) => {
	switch (type) {
		case LOGIN.PROGRESS: {
			return {
				activeLoadingIndicator: true
			};
		}
		case LOGIN.SUCCESS: {
			return {
				activeLoadingIndicator: false,
				accessToken: payload.accessToken,
				userPassword: payload.userPassword,
				loginStatus: true
			};
		}
		case LOGIN.FAILURE: {
			return {
				activeLoadingIndicator: false,
				loginStatus: false,
				errorMessage: payload.errorMessage
			};
		}
		default:
			return state;
	}
};