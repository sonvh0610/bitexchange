import { HISTORY } from '../constants';

const DEFAULT_STATE = {
    activeLoadingIndicator: false
}

export default (state = DEFAULT_STATE, { type, payload }) => {
    switch (type) {
        case HISTORY.PROGRESS: {
            return {
                activeLoadingIndicator: true
            }
        }
        case HISTORY.VIEW_SENT_SUCCESS: {
            return {
                activeLoadingIndicator: false,
                apiType: 'send',
                logData: payload.logData
            }
        }
        case HISTORY.VIEW_SENT_FAILURE: {
            return {
                activeLoadingIndicator: false,
                apiType: 'send',
                errorMessage: payload.errorMessage
            }
        }

        case HISTORY.VIEW_RECEIVED_SUCCESS: {
            return {
                activeLoadingIndicator: false,
                apiType: 'receive',
                logData: payload.logData
            }
        }
        case HISTORY.VIEW_RECEIVED_FAILURE: {
            return {
                activeLoadingIndicator: false,
                apiType: 'receive',
                errorMessage: payload.errorMessage
            }
        }
        default: {
            return state;
        }
    }
}