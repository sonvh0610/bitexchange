import { combineReducers } from 'redux';
import reducerLogin from './login';
import reducerDashboard from './dashboard';
import reducerTransferMoney from './transferMoney';
import reducerHistory from './history';

export default combineReducers({
	reducerLogin,
	reducerDashboard,
	reducerTransferMoney,
	reducerHistory
});