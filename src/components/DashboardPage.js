import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, Alert, AsyncStorage } from 'react-native';
import { SideMenu, Icon } from 'react-native-elements';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import PTRView from 'react-native-pull-to-refresh';

import { CardDashboard } from '../commons';
import { getProfile, logout } from '../actions';
import { LoadingIndicator, translate } from '../commons';

class DashboardPage extends Component {
	constructor(props) {
		super(props);
		this.state = {
			language: ''
		}
	}

	showSetting() {
		Actions.setting();
	}

	transferMoney() {
		const { accessToken, userPassword } = this.props;
		Actions.transferMoney({
			accessToken,
			userPassword
		});
	}

	showHistory() {
		const { accessToken } = this.props;
		Actions.history({ accessToken });
	}

	logout() {
		const { accessToken } = this.props;
		Alert.alert(translate('confirm', this.state.language), translate('exit_app', this.state.language),
			[
				{ text: translate('cancel', this.state.language) },
				{ text: translate('ok', this.state.language), onPress: () => { this.props.logout(accessToken); Actions.login(); } }
			]
		);
	}

	async componentWillMount() {
		const { accessToken } = this.props;
		this.props.getProfile(accessToken);

		let language = await AsyncStorage.getItem('language');
		this.setState({language});
	}

	pullToRequest = () => {
		return new Promise(resolve => {
			const { accessToken } = this.props;
			this.props.getProfile(accessToken);
			resolve();
		});
	}

	render() {
		let { userInfo } = this.props.dashboardProps;
		return (
			<View style={{flex: 1}}>
				<LoadingIndicator activeLoadingIndicator={this.props.dashboardProps.activeLoadingIndicator} />
				<PTRView onRefresh={this.pullToRequest}>
					<View style={styles.container}>
						<Image style={styles.cover} source={require('../assets/images/cover.jpg')}>
							<View style={styles.avatarCover}>
								<View style={styles.avatarContainer}>
									<Image style={styles.avatar} 
										source={require('../assets/images/logo.png')} />
								</View>
								{userInfo && 
									<View style={styles.infomation}>
										<Text style={styles.userFullName}>{userInfo.name}</Text>
										<Text style={styles.userName}>{userInfo.username}</Text>
										<Text style={styles.userMoney}>Cash: {userInfo.cash}</Text>
										<Text style={styles.userMoney}>Bec: {userInfo.bec}</Text>
									</View>
								}
							</View>
						</Image>
						<View style={styles.function}>
							<View style={styles.gridView}>
								<CardDashboard icon='exchange' text={translate('transaction', this.state.language)} iconColor='#c0392b' onClick={this.transferMoney.bind(this)} />
								<CardDashboard icon='cog' text={translate('setting', this.state.language)} iconColor='#16a085' onClick={this.showSetting.bind(this)} />
							</View>
							<View style={styles.gridView}>
								<CardDashboard icon='history' text={translate('history', this.state.language)} iconColor='#2c3e50' onClick={this.showHistory.bind(this)} />
								<CardDashboard icon='sign-out' text={translate('logout', this.state.language)} iconColor='#7f8c8d' onClick={this.logout.bind(this)} />
							</View>
						</View>
					</View>
				</PTRView>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1
	},
	cover: {
		height: 150,
		resizeMode: 'cover',
		padding: 20,
		paddingBottom: 30
	},
	function: {
		flex: 4,
		flexDirection: 'row',
		padding: 20
	},
	gridView: {
		flex: 1,
		flexDirection: 'column',
		justifyContent: 'flex-start'
	},
	avatarCover: {
		flex: 1,
		flexDirection: 'row'
	},
	avatarContainer: {
		flex: 1
	},
	avatar: {
		width: 60,
		height: 60
	},
	infomation: {
		flex: 10
	},
	userFullName: {
		color: 'white',
		fontSize: 20,
		fontWeight: 'bold',
		backgroundColor: 'transparent'
	},
	userName: {
		color: 'white',
		fontStyle: 'italic',
		marginBottom: 10,
		backgroundColor: 'transparent'
	},
	userMoney: {
		color: 'white',
		backgroundColor: 'transparent'
	}
});

const mapStateToProps = (state) => {
	return {
		dashboardProps: state.reducerDashboard
	}
}

export default connect(mapStateToProps, {getProfile, logout})(DashboardPage);