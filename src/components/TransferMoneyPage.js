import React from 'react';
import { View, Text, StyleSheet, TextInput, ActivityIndicator, Alert, AsyncStorage } from 'react-native';
import { Button, FormLabel, FormInput } from 'react-native-elements';
import { connect } from 'react-redux';

import { TextWithIcon, LoadingIndicator, translate } from '../commons';
import { transferMoney } from '../actions';

class TransferMoneyPage extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			receiveAccountName: '',
			amountBec: '',
			transactionPassword: '',
			language: ''
		}
	}

	transferMoney = () => {
		const { accessToken, userPassword } = this.props;
		this.props.transferMoney(accessToken, userPassword, this.state.transactionPassword, this.state.receiveAccountName, this.state.amountBec);
	}

	async componentWillMount() {
		let language = await AsyncStorage.getItem('language');
		this.setState({language});
	}

	componentWillReceiveProps(nextProps) {
		const { transferMoneyProps } = nextProps;
		if (!transferMoneyProps.activeLoadingIndicator) {
			if (transferMoneyProps.transferMoneyStatus) {
				Alert.alert(translate('confirm', this.state.language), translate('success_transaction', this.state.language));
			}
			else {
				if (transferMoneyProps.errorMessage)
					Alert.alert(translate('error', this.state.language), transferMoneyProps.errorMessage);
			}
		}
	}

	render() {
		return (
			<View style={{flex: 1}}>
				<LoadingIndicator activeLoadingIndicator={this.props.transferMoneyProps.activeLoadingIndicator} />
				<View style={styles.container}>
					<View style={styles.transferContainer}>
						<TextWithIcon icon='user' text={translate('receiver_account', this.state.language)} iconColor='#c0392b' />
						<TextInput placeholder={translate('receiver_account', this.state.language)}
							style={styles.textInput}
							value={this.state.receiveAccountName}
							onChangeText={(account) => this.setState({receiveAccountName: account})}/>

						<TextWithIcon icon='usd' text={translate('transaction_amount', this.state.language)} iconColor='#27ae60' />
						<TextInput placeholder={translate('transaction_amount', this.state.language)}
							style={styles.textInput}
							keyboardType='numeric'
							value={this.state.amountBec}
							onChangeText={(amountBec) => this.setState({amountBec: amountBec})} />

						<TextWithIcon icon='key' text={translate('transaction_password', this.state.language)} iconColor='#7f8c8d' />
						<TextInput placeholder={translate('transaction_password', this.state.language)}
							secureTextEntry={true}
							style={styles.textInput}
							value={this.state.transactionPassword}
							onChangeText={(password) => this.setState({transactionPassword: password})} />
					</View>
					<Button backgroundColor='#27ae60' 
						large
						icon={{name: 'check'}}
						style={styles.submitTransaction}
						title={translate('confirm_transaction', this.state.language)}
						onPress={this.transferMoney} />
				</View>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		padding: 20
	},
	header: {
		flex: 1,
		backgroundColor: '#27ae60',
		alignItems: 'center',
		justifyContent: 'center'
	},
	transferContainer: {
		flex: 7,
		justifyContent: 'flex-start'
	},
	textInput: {
		height: 40,
		marginBottom: 20
	},
	submitTransaction: {
		alignSelf: 'stretch'
	}
});

const mapStateToProps = (state) => {
	return {
		transferMoneyProps: state.reducerTransferMoney
	}
};

export default connect(mapStateToProps, {transferMoney})(TransferMoneyPage);