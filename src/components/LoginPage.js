import React from 'react';
import { StyleSheet, Text, View, Image, TextInput, Button, Alert, AsyncStorage } from 'react-native';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';

import { loginToSystem } from '../actions';
import { LoadingIndicator, translate } from '../commons';

class LoginPage extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			username: '',
			password: '',
			language: ''
		}
	}

	login() {
		this.props.loginToSystem(this.state.username, this.state.password);
	}

	async componentWillMount() {
		let language = await AsyncStorage.getItem('language');
		if (language == null) {
			language = 'vi';
			AsyncStorage.setItem('language', 'vi');
		}
		this.setState({ language });
	}

	componentWillReceiveProps(nextProps) {
		if (!nextProps.loginProps.activeLoadingIndicator) {
			if (nextProps.loginProps.loginStatus) {
				Actions.dashboard({
					accessToken: nextProps.loginProps.accessToken,
					userPassword: nextProps.loginProps.userPassword
				});
			}
			else {
				if (nextProps.loginProps.errorMessage)
					Alert.alert('Lỗi', nextProps.loginProps.errorMessage);
			}
		}
	}

	render() {
		return (
			<View style={{flex: 1}}>
				<LoadingIndicator activeLoadingIndicator={this.props.loginProps.activeLoadingIndicator} />
				<View style={styles.container}>
					{<Image style={styles.logo} source={require('../assets/images/logo.png')} />}
					<TextInput
						onChangeText={(username) => this.setState({username})}
						value={this.state.username}
						style={styles.textinput}
						placeholder={translate('username', this.state.language)}
						placeholderTextColor='white'
						underlineColorAndroid={'transparent'} />
					<TextInput
						secureTextEntry={true}
						onChangeText={(password) => this.setState({password})}
						value={this.state.password}
						style={styles.textinput}
						placeholder={translate('password', this.state.language)}
						placeholderTextColor='white'
						underlineColorAndroid={'transparent'} />
					<Button title={translate('login', this.state.language)} style={styles.button} onPress={this.login.bind(this)} />
				</View>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		alignItems: 'center',
		backgroundColor: '#2E4159',
		padding: 20
	},
	logo: {
		width: 100,
		height: 100,
		marginTop: 150,
		marginBottom: 100
	},
	textinput: {
		height: 40,
		alignSelf: 'stretch',
		backgroundColor: 'rgba(255, 255, 255, 0.2)',
		marginBottom: 20,
		color: 'white'
	},
	button: {
		alignSelf: 'stretch'
	}
});

const mapStateToProps = (state) => {
	return {
		loginProps: state.reducerLogin
	};
};

export default connect(mapStateToProps, {loginToSystem})(LoginPage);