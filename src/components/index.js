import LoginPage from './LoginPage';
import DashboardPage from './DashboardPage';
import TransferMoneyPage from './TransferMoneyPage';
import HistoryPage from './HistoryPage';
import SettingPage from './SettingPage';

export { 
	LoginPage,
	DashboardPage,
	TransferMoneyPage,
	HistoryPage,
	SettingPage
};