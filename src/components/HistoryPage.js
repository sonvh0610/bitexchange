import React from 'react';
import { StyleSheet, View, Text, ScrollView, AsyncStorage } from 'react-native';
import { Tabs, Tab, List, ListItem, Icon } from 'react-native-elements';
import { connect } from 'react-redux';
import DateTimePicker from 'react-native-modal-datetime-picker';

import { Utility, translate } from '../commons';
import { viewHistorySent, viewHistoryReceived } from '../actions';

class HistoryPage extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			fromDateTimePickerVisible: false,
			endDateTimePickerVisible: false,
			fromDateText: '',
			endDateText: '',
			selectedTab: 0,
			language: ''
		}
	}

	startDate = null;
	endDate = null;

	componentWillMount() {
		this.endDate = new Date();
		this.startDate = new Date();
		this.startDate.setMonth(this.startDate.getMonth() - 1);
		this.startDate.setDate(this.startDate.getDate() + 1);

		this.setState({
			fromDateText: Utility.convertDateToString(this.startDate),
			endDateText: Utility.convertDateToString(this.endDate),
		});

		const { accessToken } = this.props;

		this.props.viewHistoryReceived(accessToken, this.startDate, this.endDate);
	}

	async componentDidMount() {
		let language = await AsyncStorage.getItem('language');
		if (language == null) {
			language = 'vi';
			AsyncStorage.setItem('language', 'vi');
		}

		this.setState({language});
	}

	pressStartDateTimePicker = () => this.setState({ fromDateTimePickerVisible: true })
	pressEndDateTimePicker = () => this.setState({ endDateTimePickerVisible: true });

	cancelStartDateTimePicker = () => this.setState({ fromDateTimePickerVisible: false });
	cancelEndDateTimePicker = () => this.setState({ endDateTimePickerVisible: false });

	handleStartDatePicked = (date) => {
		const { accessToken } = this.props;
		this.startDate = date;
		this.props.viewHistorySent(accessToken, this.startDate, this.endDate);
		this.setState({
			fromDateText: Utility.convertDateToString(date),
			fromDateTimePickerVisible: false
		});
	}

	handleEndDatePicked = (date) => {
		const { accessToken } = this.props;
		this.endDate = date;
		this.props.viewHistorySent(accessToken, this.startDate, this.endDate);
		this.setState({
			endDateText: Utility.convertDateToString(date),
			endDateTimePickerVisible: false
		});
	}

	changeTab = (index) => {
		const { accessToken } = this.props;
		switch (index) {
			case 0: {
				this.props.viewHistoryReceived(accessToken, this.startDate, this.endDate);
				break;
			}
			case 1: {
				this.props.viewHistorySent(accessToken, this.startDate, this.endDate);								
				break;
			}
		}
		this.setState({ selectedTab: index });
	}

	render() {
		const { logData, apiType } = this.props.historyProps;
		const tabs = [
			{
				name: translate('income', this.state.language),
				icon: 'sign-in'
			},
			{
				name: translate('outcome', this.state.language),
				icon: 'sign-out'
			}
		];
		
		return (
			<View style={styles.container}>
				<List containerStyle={{marginBottom: 20}}>
					<ListItem
						title={translate('from', this.state.language)}
						subtitle={this.state.fromDateText}
						onPress={this.pressStartDateTimePicker} />
					<ListItem
						title={translate('to', this.state.language)}
						subtitle={this.state.endDateText}
						onPress={this.pressEndDateTimePicker} />
				</List>
				<DateTimePicker isVisible={this.state.fromDateTimePickerVisible}
					onConfirm={this.handleStartDatePicked}
					onCancel={this.cancelStartDateTimePicker} />
				<DateTimePicker isVisible={this.state.endDateTimePickerVisible}
					onConfirm={this.handleEndDatePicked}
					onCancel={this.cancelEndDateTimePicker} />
				<Text style={{fontWeight: 'bold', fontSize: 20, textAlign: 'center'}}>{translate('history_transaction', this.state.language)}</Text>
				<Tabs>
					{
						tabs.map((item, index) => (
							<Tab key={index}
								tabStyle={styles.tab}
								titleStyle={{color: 'rgba(0,0,0,0.5)'}}
								selectedTitleStyle={{fontWeight: 'bold', color: 'rgba(0,0,0,0.8)'}}
								selected={this.state.selectedTab == index}
								title={item.name}
								onPress={() => this.changeTab(index)}
								renderIcon={() => 
									<Icon containerStyle={{justifyContent: 'center', alignItems: 'center'}}
										name={item.icon}
										type='font-awesome'
										color='rgba(0,0,0,0.5)'
										size={20} />
									}
								renderSelectedIcon={() => 
									<Icon containerStyle={{justifyContent: 'center', alignItems: 'center'}}
										name={item.icon}
										type='font-awesome'
										color='rgba(0,0,0,0.8)'
										size={20} />
									}>
								{ logData === undefined ?
									<Text style={styles.noData}>{translate('no_data', this.state.language)}</Text> :
									<ScrollView style={{ marginTop: 10 }}>
										<List>
											{
												logData.map((item, index) =>
													apiType === 'send' ?
													<ListItem
														key={index}
														hideChevron
														title={`${item.receiverName} (${item.receiverUser})`}
														subtitle={`${item.bec} BEC`} /> :
													<ListItem
														key={index}
														hideChevron
														title={`${item.senderName} (${item.senderUser})`}
														subtitle={`${item.bec} BEC`} />
												)
											}
										</List>
									</ScrollView>
								}
							</Tab>
						))
					}
				</Tabs>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		flexDirection: 'column',
		justifyContent: 'flex-start'
	},
	noData: {
		margin: 20,
		textAlign: 'center'
	}
});

const mapStateToProps = (state) => {
	return {
		historyProps: state.reducerHistory
	}
}

export default connect(mapStateToProps, {viewHistorySent, viewHistoryReceived})(HistoryPage);