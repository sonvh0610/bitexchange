import React from 'react';
import { AsyncStorage, View, Text, StyleSheet, Alert } from 'react-native';
import { connect } from 'react-redux';
import ModalPicker from 'react-native-modal-picker'

import { translate, supportLanguage } from '../commons';

class SettingPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            language: '',
            textInputValue: ''
        }
    }

    async componentWillMount() {
        let language = await AsyncStorage.getItem('language');
		this.setState({language});
    }

    onChange = (option) => {
        AsyncStorage.setItem('language', option.key);
        this.setState({language: option.key});
        Alert.alert(translate('confirm', this.state.language), translate('restart_required', this.state.language));
    }
    
    render() {
        const data = [];

        for (var lang of supportLanguage) {
            data.push({
                key: lang,
                label: translate(lang, this.state.language)
            })
        }
 
        return (
            <View style={{paddingTop: 15}}>
                <ModalPicker
                    data={data}
                    initValue={this.state.language}
                    onChange={this.onChange}>
                    
                    <View style={styles.setting}>
                        <Text style={styles.mainText}>{translate('language', this.state.language)}</Text>
                        <Text>{translate(this.state.language, this.state.language)}</Text>
                    </View>
                    
                        
                </ModalPicker>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    setting: {
        padding: 20,
        backgroundColor: 'white'
    },
    mainText: {
        fontWeight: 'bold',
        fontSize: 18
    }
});

export default connect()(SettingPage);