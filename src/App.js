import React, { Component } from 'react';
import { View, ActivityIndicator, AsyncStorage } from 'react-native';
import { Provider } from 'react-redux';
import { Router, Scene, ActionConst, Actions } from 'react-native-router-flux';

import store from './store/store';
import {
	LoginPage,
	DashboardPage,
	TransferMoneyPage,
	HistoryPage,
	SettingPage
} from './components';
import { translate } from './commons';

export default class App extends Component {
	constructor(props) {
		super(props);
		this.state = {
			language: 'en'
		}
	}

	async componentWillMount() {
		let language = await AsyncStorage.getItem('language');
		this.setState({ language });
	}

	render() {
		return (
			<Provider store={store}>
				<Router>
					<Scene key='root'>
						<Scene key='login' component={LoginPage} hideNavBar type={ActionConst.RESET} initial />
						<Scene key='dashboard' component={DashboardPage} title={translate('dashboard', this.state.language)} type={ActionConst.RESET} />
						<Scene key='transferMoney' component={TransferMoneyPage} title={translate('transaction', this.state.language)} />
						<Scene key='history' component={HistoryPage} title={translate('history', this.state.language)} />
						<Scene key='setting' component={SettingPage} title={translate('setting', this.state.language)} />
					</Scene>
				</Router>
			</Provider>
		);
	}
}