import { DASHBOARD } from '../constants';
import { ApiInterface } from '../services/api';

export function getProfileProcess() {
	return {
		type: DASHBOARD.GET_USER_PROCESS
	}
}

export function getProfileSuccess(userInfo) {
	return {
		type: DASHBOARD.GET_USER_SUCCESS,
		payload: {
			userInfo
		}
	}
}

export function getProfileFail(errorMessage) {
	return {
		type: DASHBOARD.GET_USER_FAILED,
		payload: {
			errorMessage
		}
	}
}

export function getProfile(accessToken) {
	return function(dispatch) {
		dispatch(getProfileProcess());
		
		return ApiInterface.getUserInfo(accessToken)
			.then(userInfo => {
				dispatch(getProfileSuccess(userInfo));
			})
			.catch(error => {
				dispatch(getProfileFail(error.message))
			});
	};
}

export function logoutProcess() {
	return {
		type: DASHBOARD.LOGOUT_PROCESS
	}
}

export function logoutProcessSuccess() {
	return {
		type: DASHBOARD.LOGOUT_SUCCESS
	}
}

export function logoutProcessFail(errorMessage) {
	return {
		type: DASHBOARD.LOGOUT_FAILURE,
		payload: {
			errorMessage
		}
	}
}

export function logout(accessToken) {
	return function(dispatch) {
		dispatch(logoutProcess());

		return ApiInterface.logout(accessToken)
			.then(result => {
				dispatch(logoutProcessSuccess());
			})
			.catch(error => {
				dispatch(logoutProcessFail(error.message));
			});
	}
}