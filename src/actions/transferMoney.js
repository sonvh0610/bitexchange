import { TRANSER_MONEY } from '../constants';
import { ApiInterface } from '../services/api';

export function transfer() {
	return {
		type: TRANSER_MONEY.PROGRESS
	}
}

export function transferMoneySuccess() {
	return {
		type: TRANSER_MONEY.SUCCESS
	}
}

export function transferMoneyFail(errorMessage) {
	return {
		type: TRANSER_MONEY.FAILURE,
		payload: {
			errorMessage
		}
	}
}

export function transferMoney(accessToken, userPassword, transactionPassword, receiver, amount) {
	return function(dispatch) {
		dispatch(transfer());

		return ApiInterface.sendBecCoin(accessToken, userPassword, transactionPassword, receiver, amount)
			.then(result => {
				dispatch(transferMoneySuccess());
			})
			.catch(error => {
				dispatch(transferMoneyFail(error.message));
			});
	}
};