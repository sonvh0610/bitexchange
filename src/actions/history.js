import { HISTORY } from '../constants';
import { ApiInterface } from '../services/api';

export function viewHistoryProcess() {
    return {
        type: HISTORY.PROGRESS
    }
}

export function viewHistorySentSuccess(logData) {
    return {
        type: HISTORY.VIEW_SENT_SUCCESS,
        payload: {
            logData
        }
    }
}

export function viewHistorySentFailure(errorMessage) {
    return {
        type: HISTORY.VIEW_SENT_FAILURE,
        payload: {
            errorMessage
        }
    }
}

export function viewHistorySent(accessToken, startDate, endDate) {
    return function(dispatch) {
        dispatch(viewHistoryProcess());

        return ApiInterface.viewHistorySent(accessToken, startDate, endDate)
            .then(logData => {
                dispatch(viewHistorySentSuccess(logData))
            })
            .catch(error => {
                dispatch(viewHistorySentFailure(error.message))
            });
    }
}

export function viewHistoryReceivedSuccess(logData) {
    return {
        type: HISTORY.VIEW_RECEIVED_SUCCESS,
        payload: {
            logData
        }
    }
}

export function viewHistoryReceivedFailure(errorMessage) {
    return {
        type: HISTORY.VIEW_RECEIVED_FAILURE,
        payload: {
            errorMessage
        }
    }
}

export function viewHistoryReceived(accessToken, startDate, endDate) {
    return function(dispatch) {
        dispatch(viewHistoryProcess());

        return ApiInterface.viewHistoryReceived(accessToken, startDate, endDate)
            .then(logData => {
                dispatch(viewHistoryReceivedSuccess(logData))
            })
            .catch(error => {
                dispatch(viewHistoryReceivedFailure(error.message))
            });
    }
}