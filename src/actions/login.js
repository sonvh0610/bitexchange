import { LOGIN } from '../constants';
import md5 from 'md5';
import { ApiInterface } from '../services/api';

export function login() {
	return {
		type: LOGIN.PROGRESS
	}
}

export function loginSuccess(accessToken, password) {
	return {
		type: LOGIN.SUCCESS,
		payload: {
			accessToken,
			userPassword: md5(password)
		}
	}
}

export function loginFail(errorMessage) {
	return {
		type: LOGIN.FAILURE,
		payload: {
			errorMessage
		}
	}
}

export function loginToSystem(username, password) {
	return function(dispatch) {
		dispatch(login());

		return ApiInterface.login(username, password).then(accessToken => {
			dispatch(loginSuccess(accessToken, password));
		})
		.catch(error => {
			dispatch(loginFail(error.message));
		})
	};
}