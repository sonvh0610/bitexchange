import React from 'react';
import { View, StyleSheet, Text, TouchableOpacity } from 'react-native';
import { Card } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';

export class CardDashboard extends React.Component {
	render() {
		let { icon, iconColor, onClick, text } = this.props;
		let view = (
			<Card>
				<View style={styles.container}>
					<Icon style={styles.icon} name={icon} size={40} color={iconColor} />
					<Text style={styles.text}>{text}</Text>
				</View>
			</Card>
		);
		
		if (onClick) {
			return (
				<TouchableOpacity onPress={onClick.bind(this)}>
					{ view }
				</TouchableOpacity>
			);
		}
		else {
			return view;
		}
	}
}

const styles = StyleSheet.create({
	container: {
		alignItems: 'center'
	},
	icon: {
	},
	text: {
		fontWeight: 'bold'
	}
});