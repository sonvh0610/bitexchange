import viLang from '../assets/languages/vi.json';
import enLang from '../assets/languages/en.json';

export const supportLanguage = ['vi', 'en'];

export function translate(keyword, language) {
    if (supportLanguage.indexOf(language) != -1) {
        switch (language) {
            case 'vi': {
                return viLang[keyword] || keyword;
            }
            case 'en': {
                return enLang[keyword] || keyword;
            }
        }
    }
    else {
        return viLang[keyword] || keyword;
    }
}