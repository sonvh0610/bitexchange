import React from 'react';
import { View, StyleSheet, Text, TouchableOpacity } from 'react-native';
import { Card } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';

export class TextWithIcon extends React.Component {
	render() {
		let { onClick } = this.props;

		let view = (
			<View style={styles.container}>
				<Icon style={styles.icon} name={this.props.icon} size={20} color={this.props.iconColor} />
				<Text style={styles.text}>{this.props.text}</Text>
			</View>
		);
		
		if (onClick) {
			return (
				<TouchableOpacity onPress={onClick.bind(this)}>
					{ view }
				</TouchableOpacity>
			);
		}
		else {
			return view;
		}
	}
}

const styles = StyleSheet.create({
	container: {
		flexDirection: 'row',
		alignItems: 'center'
	},
	icon: {
		flex: 1,
	},
	text: {
		flex: 8,
		fontWeight: 'bold'
	}
});