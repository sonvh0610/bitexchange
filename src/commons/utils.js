export class Utility {
	static convertDateToString(date) {
		return `${date.getDate()}/${date.getMonth() + 1}/${date.getFullYear()}`;
	}
}