import React from 'react';
import { View, StyleSheet, ActivityIndicator, Dimensions } from 'react-native';

export class LoadingIndicator extends React.Component {
	render() {
		const { width, height } = Dimensions.get('window');
		let displayTop = (this.props.activeLoadingIndicator) ? 0 : -(height * 2);
		return (
			<View style={{
					position: 'absolute',
					zIndex: 999,
					alignItems: 'center',
					backgroundColor: 'rgba(0,0,0,0.6)',
					width,
					height,
					top: displayTop
				}}>
				<ActivityIndicator animating={this.props.activeLoadingIndicator}
					color='#bc2b78'
					size="large"
					style={{
						position: 'absolute',
						zIndex: 999,
						top: height / 2 - 100
					}} />
			</View>
		);
	}
}